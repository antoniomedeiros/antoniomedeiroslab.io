---
date: '2025-02-24 23:20:00 GMT-3'
image: '/files/2025/02/youtube.jpg'
layout: post
title: 'SaveFrom.net: o jeito mais fácil de baixar vídeos do YouTube'
excerpt: 'Você já quis ou precisou baixar vídeos do YouTube? Seja para assistir um vídeo durante uma viagem (onde nem sempre você tem sinal de Internet), seja para guardar consigo uma cópia dos vídeos que você publicou, ou qualquer que seja a sua situação, baixar vídeos do YouTube pode ser bastante conveniente. Existem vários aplicativos e serviços que você pode usar para baixar vídeos do YouTube. Mas a forma mais simples e fácil de fazer isso, sem precisar baixar nem instalar nenhum programa, é usar o serviço online SaveFrom.net, diretamente do seu navegador preferido.'
---

{% include image.html src='/files/2025/02/youtube.jpg' caption='Imagem por [@natanaelginting / Freepik](https://br.freepik.com/fotos-gratis/pilha-de-logotipos-de-botao-de-reproducao-3d_1191373.htm)' %}

Você já quis ou precisou baixar vídeos do [YouTube]? Seja para assistir um vídeo durante uma viagem (onde nem sempre você tem sinal de Internet), seja para guardar consigo uma cópia dos vídeos que você publicou, ou qualquer que seja a sua situação, baixar vídeos do YouTube pode ser bastante conveniente.

Existem vários aplicativos e serviços que você pode usar para baixar vídeos do YouTube. Mas a forma mais simples e fácil de fazer isso, sem precisar baixar nem instalar nenhum programa, é usar o serviço _online_ **[SaveFrom.net]**, diretamente do seu navegador preferido.

Vou te mostrar como você pode usar esse serviço. Aqui, estou usando meu _notebook_ com [Linux Kamarada 15.6], mas note que você pode usar o SaveFrom.net em qualquer dispositivo com [Windows], [Android], [iOS], etc.

[YouTube]:              https://www.youtube.com
[SaveFrom.net]:         https://savefrom.net/
[Linux Kamarada 15.6]:  https://linuxkamarada.com/pt/2024/12/18/linux-kamarada-15-6-beta-ajude-a-testar-a-melhor-versao-da-distribuicao/
[Windows]:              https://www.microsoft.com/pt-br/windows/
[Android]:              https://www.android.com/intl/pt_br/
[iOS]:                  https://www.apple.com/br/ios/

<div class="alert alert-warning" role="alert">
{% markdown %}

**Isenção de responsabilidade:** este artigo foi escrito e publicado estritamente para fins informativos. Baixar vídeos de _sites_ pode ser contra seus termos de uso e/ou violar os direitos autorais de quem produziu e/ou publicou os vídeos. Se você não tem certeza se um vídeo é protegido por direitos autorais, é melhor pecar pelo excesso de cautela e não baixá-lo. Cabe a você decidir se quer/deve (ou não) baixar vídeos. Use a tecnologia apresentada neste artigo de forma legal e responsável. O autor não aceita qualquer responsabilidade por eventuais danos resultantes da utilização das informações aqui contidas de forma indevida. Se quiser saber mais sobre o assunto, sugiro a leitura:

- [É crime baixar filme online? O que a lei diz e o que de fato acontece? - Tilt - UOL][uol]

[uol]: https://www.uol.com.br/tilt/noticias/redacao/2022/08/16/e-crime-baixar-filmes-online-o-que-a-lei-diz-e-o-que-realmente-acontece.htm

{% endmarkdown %}
</div>

Continuando... por exemplo, suponhamos que você queira baixar este vídeo:

- [Unboxing e teste do PinePhone - o smartphone que roda Linux - Linux Kamarada](https://www.youtube.com/watch?v=Jo9nL-p2J6I)

{% include image.html src='/files/2025/02/savefrom-01.jpg' %}

Na barra de endereços, adicione `ss` na frente de `youtube.com` e tecle **Enter**:

{% include image.html src='/files/2025/02/savefrom-02.png' %}

A página do serviço [SaveFrom.net] é aberta, basta clicar em **Baixar**:

{% include image.html src='/files/2025/02/savefrom-03.jpg' %}

Agora é só aguardar alguns segundos ou minutos, dependendo da velocidade da sua conexão, e pronto: o vídeo já estará na memória do seu dispositivo e poderá ser assistido _offline_. Espero que essa dica tenha sido útil. Faça bom proveito!